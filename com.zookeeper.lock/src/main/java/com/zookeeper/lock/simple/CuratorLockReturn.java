package com.zookeeper.lock.simple;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 带返回值的分布式锁
 * @ClassName: CuratorLockReturn 
 * @Description:  
 * @author ybw 
 * @date 2017年4月7日 下午5:26:12 
 */
public class CuratorLockReturn {
	private static final Logger logger=LoggerFactory.getLogger(CuratorLockReturn.class);
	/**
	 * @param args
	 * @throws InterruptedException
	 * @throws ExecutionException 
	 */
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		final int size=5;
		
		String zookeeperConnectionString = "localhost:2181";
		RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
		CuratorFramework client = CuratorFrameworkFactory.newClient(zookeeperConnectionString, retryPolicy);
		client.start();
		logger.info("客户端启动。。。。");
		ExecutorService exec = Executors.newCachedThreadPool();
		CompletionService<String> cs = new ExecutorCompletionService<String>(exec);
		for (int i = 0; i < size; i++) {
			cs.submit(new MyLock("client" + i, client));
		}
		for(int i=0;i<size;i++){
			logger.info("future:{}",cs.take().get());
		}
		exec.shutdown();
		logger.info("所有任务执行完毕");
		client.close();
		logger.info("客户端关闭。。。。");
	}

	static class MyLock implements Callable<String> {
		private String name;
		private CuratorFramework client;

		public MyLock(String name, CuratorFramework client) {
			this.name = name;
			this.client = client;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String call() throws Exception {
			InterProcessMutex lock = new InterProcessMutex(client, "/lock_mutex");
			try {
				if (lock.acquire(120, TimeUnit.SECONDS)) {
					try {
						logger.info("----------" + this.name + "获得资源----------");
						logger.info("----------" + this.name + "正在处理资源----------");
						TimeUnit.SECONDS.sleep(5);
						logger.info("----------" + this.name + "资源使用完毕----------");
					} finally {
						lock.release();
						logger.info("----------" + this.name + "释放----------");
					}
				}
			} catch (Exception e) {
				logger.error("分布式锁异常",e);
			}
			return this.name;
		}
	}
}